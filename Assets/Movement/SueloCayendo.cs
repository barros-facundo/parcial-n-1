using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SueloCayendo : MonoBehaviour
{
    private Rigidbody rb;
    private bool cayendo = false;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.isKinematic = true; 
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player") && !cayendo)
        {
            rb.isKinematic = false; 
            cayendo = true; 
        }
    }
}





