using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParedRotativa : MonoBehaviour
{
    public Transform centro;
    public float velocidadRotacion = 60.0f;
    public float radio = 5.0f;
    public float fuerzaEmpuje = 10.0f;
    public Transform puntoDeInicio; 

    private Vector3 posicionInicial;
    private float angulo = 0;

    void Start()
    {
        posicionInicial = transform.position;
    }

    void Update()
    {
        angulo += velocidadRotacion * Time.deltaTime;
        var offset = new Vector3(Mathf.Sin(angulo), 0, Mathf.Cos(angulo)) * radio;
        transform.position = centro.position + offset;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Rigidbody playerRigidbody = collision.gameObject.GetComponent<Rigidbody>();
            if (playerRigidbody != null)
            {
                Vector3 direccionEmpuje = collision.contacts[0].point - transform.position;
                playerRigidbody.AddForce(direccionEmpuje.normalized * fuerzaEmpuje, ForceMode.Impulse);

                collision.gameObject.transform.position = puntoDeInicio.position;
            }
        }
    }
}

