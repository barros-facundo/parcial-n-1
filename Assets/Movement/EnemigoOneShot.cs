using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoOneShot : MonoBehaviour
{
    public float distanciaReinicio = 2.0f;
    public Transform puntoDeInicio;
    public float umbralDireccion = 45.0f;

    private GameObject jugador;

    void Start()
    {
        jugador = GameObject.FindGameObjectWithTag("Player");
    }

    void Update()
    {
        if (jugador != null)
        {
            float distancia = Vector3.Distance(transform.position, jugador.transform.position);

            if (distancia < distanciaReinicio)
            {
                Vector3 direccionEnemigoAJugador = (jugador.transform.position - transform.position).normalized;

                Vector3 direccionMiraJugador = jugador.transform.forward;

                float angulo = Vector3.Angle(direccionMiraJugador, direccionEnemigoAJugador);

                if (angulo > umbralDireccion)
                {
                    jugador.transform.position = puntoDeInicio.position;
                }
            }
        }
    }
}
