using UnityEngine;
using UnityEngine.UI;

public class Texto : MonoBehaviour
{
    public float tiempoDeVida = 5.0f;

    private Text texto;

    void Start()
    {
        texto = GetComponent<Text>();
        Invoke("DesactivarTexto", tiempoDeVida);
    }

    void DesactivarTexto()
    {
        texto.enabled = false;
    }
}