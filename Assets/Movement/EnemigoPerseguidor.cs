using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoPerseguidor : MonoBehaviour
{
    public Transform jugador;
    public float velocidad = 2.0f;
    public Transform puntoDeInicio;

    void Update()
    {
        transform.LookAt(jugador);
        transform.Translate(Vector3.forward * velocidad * Time.deltaTime);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.transform.position = puntoDeInicio.position;
        }
    }
}

