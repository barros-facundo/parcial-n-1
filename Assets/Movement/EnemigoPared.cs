using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoPared : MonoBehaviour
{
    public Transform puntoA;
    public Transform puntoB;
    public float velocidad = 1.0f;

    public Transform puntoDeInicio;

    private Transform destinoActual;

    void Start()
    {
        destinoActual = puntoA;
    }

    void Update()
    {
        transform.LookAt(destinoActual);
        transform.Translate(Vector3.forward * velocidad * Time.deltaTime);

        if (Vector3.Distance(transform.position, destinoActual.position) < 0.1f)
        {
            destinoActual = (destinoActual == puntoA) ? puntoB : puntoA;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.transform.position = puntoDeInicio.position;
        }
    }
}

