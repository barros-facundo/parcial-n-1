using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffRecolectable : MonoBehaviour
{
    public float aumentoDeVelocidad = 2.0f; 

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            MovimientoDelPersonaje movimientoDelPersonaje = other.GetComponent<MovimientoDelPersonaje>();
            if (movimientoDelPersonaje != null)
            {
                movimientoDelPersonaje.ApplySpeedBuff(aumentoDeVelocidad);
            }

            gameObject.SetActive(false);
        }
    }
}