using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DobleSalto : MonoBehaviour
{
    public float fuerzaSalto = 10.0f;
    public int maxSaltos = 2; 

    private int saltosRestantes;
    private bool enSuelo;
    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        saltosRestantes = maxSaltos;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && saltosRestantes > 0)
        {
            Salto();
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Suelo"))
        {
            saltosRestantes = maxSaltos; 
        }
    }

    void Salto()
    {
        rb.AddForce(Vector3.up * fuerzaSalto, ForceMode.VelocityChange);
        saltosRestantes--;
    }
}
