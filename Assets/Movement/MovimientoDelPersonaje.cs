using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoDelPersonaje : MonoBehaviour
{
    public float rapidezDesplazamiento = 10.0f;
    public Transform puntoDeInicio;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Cursor.lockState = CursorLockMode.None;
        }

        if (transform.position.y < -10f)
        {
            RealizarRespawn();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Cursor.lockState = CursorLockMode.None;
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            RealizarRespawn();
        }
    }

    void RealizarRespawn()
    {
        if (puntoDeInicio != null)
        {
            transform.position = puntoDeInicio.position;
        }
    }

    public void ApplySpeedBuff(float factorDeAumento)
    {
        // Aumentar la velocidad temporalmente
        rapidezDesplazamiento *= factorDeAumento;
    }
}


